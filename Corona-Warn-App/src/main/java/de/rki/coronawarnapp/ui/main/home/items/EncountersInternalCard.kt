package de.rki.coronawarnapp.ui.main.home.items

import android.annotation.SuppressLint
import android.view.View
import android.view.ViewGroup
import de.rki.coronawarnapp.R
import de.rki.coronawarnapp.databinding.HomeEncountersInternalCardLayoutBinding
import de.rki.coronawarnapp.ui.main.home.HomeAdapter
import de.rki.coronawarnapp.ui.main.home.items.EncountersInternalCard.Item
import de.rki.coronawarnapp.util.ContextExtensions.getColorCompat
import de.rki.coronawarnapp.util.lists.diffutil.HasPayloadDiffer
import java.time.LocalDate
import java.time.format.TextStyle
import java.util.Locale

class EncountersInternalCard(parent: ViewGroup) : HomeAdapter.HomeItemVH<Item, HomeEncountersInternalCardLayoutBinding>(
    R.layout.home_card_container_layout, parent
) {

    override val viewBinding = lazy {
        HomeEncountersInternalCardLayoutBinding.inflate(
            layoutInflater,
            itemView.findViewById(R.id.card_container),
            true
        )
    }

    @SuppressLint("ClickableViewAccessibility") // chart is not focusable
    override val onBindData: HomeEncountersInternalCardLayoutBinding.(
        item: Item,
        payloads: List<Any>
    ) -> Unit = { item, payloads ->

        val onClickListener = View.OnClickListener {
            val curItem = payloads.filterIsInstance<Item>().singleOrNull() ?: item
            curItem.onClickAction(item)
        }

        itemView.setOnClickListener(onClickListener)

        val map = linkedMapOf<String, Float>()

        val today = LocalDate.now().toEpochDay()
        for (key in item.rpiHistogram) {

            if (today - key.key < 5) {
                map[
                    LocalDate.ofEpochDay(key.key).dayOfWeek.getDisplayName(
                        TextStyle.SHORT,
                        Locale.getDefault()
                    )
                ] = key.value.toFloat()
            }
        }
        chart.labelsFormatter = { "" }

        // Workaround https://codeberg.org/corona-contact-tracing-germany/cwa-android/issues/185#issuecomment-282580
        if (map.size > 0) {
            chart.barsColor = context.getColorCompat(R.color.colorPrimary)
        }

        chart.show(map)

        // Redirect chart touches to card itself
        chart.setOnTouchListener { _, event -> itemView.onTouchEvent(event) }
    }

    data class Item(
        val rpiHistogram: Map<Long, Long>,
        val onClickAction: (Item) -> Unit
    ) : HomeItem, HasPayloadDiffer {
        override val stableId: Long = Item::class.java.name.hashCode().toLong()

        override fun diffPayload(old: Any, new: Any): Any? = if (old::class == new::class) new else null
    }
}
