* Lots of upstream changes, most importantly switching to Exposure Window mode (ENF v2)
* No more gray cards and minimum tracing time. The app will report a risk right away now
* Make the microG UI for exposures available (through the ENF version number in App Information)
* Allow exporting the exposure db for analysis in another app
* Fix for exposure checks sometimes taking very long
* Directly ask for battery optimization exception instead of just sending to settings
