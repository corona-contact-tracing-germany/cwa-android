package de.rki.coronawarnapp.datadonation.safetynet

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.CommonStatusCodes
import dagger.Reusable
import de.rki.coronawarnapp.datadonation.safetynet.SafetyNetException.Type
import de.rki.coronawarnapp.environment.EnvironmentSetup
import kotlinx.coroutines.TimeoutCancellationException
import kotlinx.coroutines.suspendCancellableCoroutine
import kotlinx.coroutines.withTimeout
import okio.ByteString
import okio.ByteString.Companion.decodeBase64
import timber.log.Timber
import javax.inject.Inject
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException

@Reusable
class SafetyNetClientWrapper @Inject constructor(
    private val environmentSetup: EnvironmentSetup
) {

    suspend fun attest(nonce: ByteArray): Report {
        throw SafetyNetException(Type.ATTESTATION_REQUEST_FAILED, "Attestation not available")
    }

    private fun String.decodeBase64Json(): JsonNode {
        val rawJson = decodeBase64()!!.string(Charsets.UTF_8)
        return ObjectMapper().readTree(rawJson)
    }

    private suspend fun callClient(nonce: ByteArray): Unit =
        suspendCancellableCoroutine { cont -> }

    data class Report(
        val jwsResult: String,
        val header: JsonNode,
        val body: JsonNode,
        val signature: ByteArray
    ) {
        val nonce: ByteString? = body.get("nonce")?.asText()?.decodeBase64()

        val apkPackageName: String? = body.get("apkPackageName")?.asText()

        val basicIntegrity: Boolean = body.get("basicIntegrity")?.asBoolean() == true
        val ctsProfileMatch = body.get("ctsProfileMatch")?.asBoolean() == true

        val evaluationTypes = body.get("evaluationType")?.asText()
            ?.split(",")
            ?.map { it.trim() }
            ?: emptyList()

        val error: String? = body.get("error")?.asText()
        val advice: String? = body.get("advice")?.asText()

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false
            other as Report
            if (jwsResult != other.jwsResult) return false
            return true
        }

        override fun hashCode(): Int = jwsResult.hashCode()
    }

    companion object {
        private const val TAG = "SafetyNetWrapper"
    }
}
