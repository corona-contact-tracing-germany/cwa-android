package de.rki.coronawarnapp.qrcode.parser

import android.graphics.ImageFormat
import androidx.camera.core.ImageProxy
import com.google.zxing.BarcodeFormat
import com.google.zxing.BinaryBitmap
import com.google.zxing.DecodeHintType
import com.google.zxing.MultiFormatReader
import com.google.zxing.NotFoundException
import com.google.zxing.PlanarYUVLuminanceSource
import com.google.zxing.Result
import com.google.zxing.common.HybridBinarizer
import de.rki.coronawarnapp.tag
import timber.log.Timber
import java.nio.ByteBuffer

class QrCodeZxingParser {

    fun parseQrCode(imageProxy: ImageProxy): QrCodeBoofCVParser.ParseResult = try {
        imageProxy
            .toBinaryBitmap()
            .parse()
            .toParseResultSuccess()
    } catch (e: NotFoundException) {
        e.toParseResultSuccess()
    } catch (e: Exception) {
        Timber.tag(TAG).e(e, "Failed to parse image proxy=%s", imageProxy)
        e.toParseResultFailure()
    }

    private fun Result.toParseResultSuccess() = QrCodeBoofCVParser.ParseResult.Success(rawResults = setOf(text))
    private fun NotFoundException.toParseResultSuccess() = QrCodeBoofCVParser.ParseResult.Success(rawResults = emptySet())
    private fun Exception.toParseResultFailure() = QrCodeBoofCVParser.ParseResult.Failure(exception = this)

    // With parts from https://medium.com/@msasikanth/qr-scanning-using-camerax-4757ed3687f8
    private fun ImageProxy.toBinaryBitmap(): BinaryBitmap {
        if (format != ImageFormat.YUV_420_888) throw RuntimeException("Unexpected format")

        val source = PlanarYUVLuminanceSource(
            planes[0].buffer.toByteArray(),
            width,
            height,
            0,
            0,
            width,
            height,
            false
        )

        return BinaryBitmap(HybridBinarizer(source))
    }

    private fun BinaryBitmap.parse(): Result {
        val reader = MultiFormatReader().apply {
            val map = mapOf(
                DecodeHintType.POSSIBLE_FORMATS to arrayListOf(BarcodeFormat.QR_CODE)
            )
            setHints(map)
        }

        return reader.decode(this);
    }

    private fun ByteBuffer.toByteArray(): ByteArray {
        rewind()
        val data = ByteArray(remaining())
        get(data)
        return data
    }

    companion object {
        private val TAG = tag<QrCodeZxingParser>()
    }
}
