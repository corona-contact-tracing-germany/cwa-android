### 3.1.1.1

* Fix exposure notifications while Google Play services is installed (#285)

### 3.1.1.0

* Prepare rampdown (CWA/CCTG)
* New pinned certificate (CWA)

### 3.0.1.0

* Deprecate TAN hotline (CWA)

SafteyNet attestation is required to certify that the original, unmodified CWA app is used to Warn Others after positive self tests or after positive test results not provided to the app. Therefore this functionality is not available in CCTG.

### 2.28.3.1

* fixed data collection onboarding screen being accidentally shown
* fixed one occurance of CWA in release info strings

### 2.28.3.0

* New stats tile links to RKI website (CWA)

### 2.27.2.0

* Show mask mandate (CWA)

### 2.26.0.0

* Show the correct time last exposure key update (This was off by one our into the past, so we actually had data for one hour later than we did show in the UI)
* Revert using a translucent status bar to ease maintenance and fix a few visual bugs
* Try minifying our releases again to reduce app size
* Align dgc-certlogic-android and dgc-business-rules with upstream again after we enabled desugaring in the last release.
* Update our microG version to include the "Show weekday for reported exposures" commit
* Fix Rapid test profile crashes on starting code scanner (#275)
* Bug fixes and usability improvements from CWA 2.26

### 2.25.0.0

* Updated guidelines in case of a low or increased risk
* General bug fixes and usability improvements

### 2.24.2.2

* Fix reproducibility issues (part 1?)

### 2.24.2.0

* Export all certificates as PDF at once (CWA)

### 2.23.2.0

* Enable reissue of certificates (CWA)
* Family RAT profiles (CWA)

### 2.21.1.2

* Properly fix exposure notifications not working on Android 12 with external microG (#259)

### 2.21.1.1

* Fix exposure notifications not working on Android 12 with external microG (#259)

### 2.21.1.0

* Registration of Rapid PCR Tests (CWA)
* Registration of multiple tests for family members (CWA)
* Error-tolerant certificate grouping (CWA)
* Support certificate update to new schema version (CWA)
* Expire high risk sooner (CWA)
* Don't hide risk card while tested PCR-positive (CWA)
* Add notifications on status change and on new exposure (CWA)
* No validity date for recovery certificate (CWA)
* Duplicate QR Code Removed (CWA)
* Material Design 3 (CWA)
* Don't request location permission on Android 12
* Fix timezone issues in risk card

### 2.18.1.0

* Add manual risk checks and overhaul UI regarding risk checks
* DCC ticketing ([why?](https://codeberg.org/corona-contact-tracing-germany/cwa-android/src/branch/main/docs/dcc-ticketing.md)) (CWA)
* Better 2G+ support: badge display via central rules and show tests quickly (CWA)
* Better scanner (CWA)
* Revised information upon test result (CWA)
* New statistics: booster shots (CWA)
* Mastodon link (like CWA)
* Recycle bin: show deletion date (CWA)
* Notification upon risk change while in app (CWA)

### 2.14.1.4-dev

* Test release to try out refreshing risk status more often than CWA
* Add a manual refresh action in the risk status card

### 2.14.1.3

* Update microG to fix another bug with risk calculation (https://codeberg.org/corona-contact-tracing-germany/cwa-android/issues/228)

### 2.14.1.2

* [Risk calculation fixes](https://codeberg.org/corona-contact-tracing-germany/cwa-android/src/branch/main/docs/risk-calculation-issue.md)
* Open handable links if scanned via NFC (thanks @dennisguse)
* Fix F-Droid reproducibility issue again
* Fix crash with booster notifications on old Android versions

### 2.14.1.1

* Fix logic for encounters card (scanning vs advertising)
* Mention cctg privacy contact in addition to RKI 
* Some wording improvements, fixes
* Try to fix fdroid reproducibility issue

### 2.14.1.0

* New statistics tiles (7-day hospitalization incidence, COVID-19 patients in intensive care units) (CWA)
* Be more error tolerant in assigning certificates to people despite minor spelling variations (CWA)
* Removed certificates and test results can be restored from a trash bin now (CWA)
* Universal QR-code scanner in the bottom bar (CWA)
* Add a dismissable card warning about root access on device (CCTG)
* Add a card showing the relative number of recent encounters during the last few days (CCTG)
* Implement an update check via our F-Droid repository if CWA server updates the minimumAppVersion (CCTG)
* Fix asking for Nearby permission on Android 12 (microG)
* Improve background location permission granting on Android 11 & 12 (microG)
* Verify signature on downloaded exposure file (microG)
* Alert about invalidated certificates (CWA)
* Improve priority rules for certificates to acccount for booster vaccinations (CWA)


### 2.11.2.0

* Universal QR code scanner (CWA)
* Scan QR code from document (CWA)

### 2.10.1.0

* Preparations for booster vaccination reminders (CWA)
* Export certificate as PDF (CWA)

### 2.9.0.0

* Warn for Others: Organizers may warn as a substitute for an infected person if they don't use CWA (CWA)
* Link to test certificate from test result screen (CWA)

### 2.8.0.0

* Certificate usability improvements (show standardized names, open from test result) (CWA)
* Display certificate expiry date and verify electronic signature (CWA 2.7.x)
* Fix Android 11 ENF permission prompt (microG)

### 2.6.1.0

* Test certificate against country's business rules (CWA)
* Local incidence cards (CWA)
* Edit RAT profile (CWA)

### 2.5.1.0

* Recovery certificate (CWA)
* Certificates for multiple persons (CWA)
* More statistics (CWA)
* Clarify error reporting screen texts

### 2.4.3.0

* Add official digital test certificates to the app (CWA)
* Show test results in contact diary (CWA)
* Fix off-by-one error in vaccination certificate validity calculation (CWA)
* Mention error reporting functionality in our privacy policy
* Link to upstream FAQ entry for incompatibility warning

### 2.3.4.1

* Add proof of vaccination (CWA)

### 2.2.1.1

* Enable resource and code shrinking (reduces apk size by another ~25%)
* Fix non-reproducible build
* Minor UI and text adjustments

### 2.2.1.0

* Create rapid test profiles to share your data at test points more quickly (CWA)
* Various bug fixes and improvements (CWA)

### 2.1.2.0

* Register Rapid Antigen Tests (CWA)
* Negative RAT proof (CWA)
* Display international phone numbers (CWA)

### 2.0.5.0

* Friendly error message when scanning invalid code (CWA)
* Better networking behavior (CWA)

### 2.0.3.0

* Check-in selection in submission flow (CWA)
* Fix white/black screen when adding a new person to the contact diary
* Some more minor bug fixes

### 2.0.2.0-rc1

* Add event check-in functionality via QR codes (CWA)
* Create QR codes for your event from within the app (CWA)
* Add a warning card on the main screen when battery optimizations are enabled
* Fixed some more visual glitches on Android 5
* Reduce apk size by not prerendering all vector graphics

### 1.15.1.0

* Add Switzerland to transnational exposure logging (upstream CWA)
* Adjust risk tiles (upstream CWA)

### 1.14.3.0

* Contact journal can now record additional information (duration, etc.) (upstream CWA)
* Add contact journal launcher shortcut (upstream CWA)
* Some more details about the risk status are now displayed (upstream CWA)
* Show encounters bottom tab icon for external microG (pending [upstream support](https://github.com/microg/GmsCore/pull/1416))

### 1.13.2.3

* Properly detect external/internal microG version on /e/ ROM

### 1.13.2.2

* Remove faulty GMS detection code.

### 1.13.2.1

* Fix a crash occurring on startup on some devices.

### 1.13.2.0

* New tab based navigation on the home screen for accessing the contact journal and the exposure history (only with builtin microG)
* Exposure logging can now be turned back on again after a positive test. (upstream CWA)
* Reworked microG detection code, opening the correct microG instance (internal vs external) should now always work.
* Show a warning on the home screen in case your phone isn't fully compatible with contact tracing.
* Enable debug log recording functionality in production builds (can be found in App Information).

**NOTE:** Upstream CWA's data donation and survey features won't be available in CCTG until further notice ([CWA Wishlist ticket](https://github.com/corona-warn-app/cwa-wishlist/issues/356))

### 1.12.0.0

* Display exposure history in the contact journal (upstream CWA)
* Release information screen in the app (upstream CWA)
* Lots of minor bug fixes (see [upstream changelog](https://github.com/corona-warn-app/cwa-app-android/releases/tag/v1.12.0) for details)

### 1.11.0.8

* reworked main screen (upstream CWA)
* new statistics cards (upstream CWA)
* reworked collected IDs chart (upstream microg)
* translucent status bar

### 1.10.1.1

* Make exposure notification settings screen more accessible (Thanks @pr0gr8mm3r !)

### 1.10.1.0

* Fix a bug where you could get stuck on the launcher activity after upgrading microG
* Add Bulgarian Translation for the donation card
* Add German FAQ page (linked when the app is used in German)
* Changes from CWA 1.10.1:
  * Implement a Contact Diary
  * Fix days since last encounter being displayed incorrectly

### 1.9.1.6

* Fix microG version check on /e/ ROMs (/e/ has two possible microG versions, one with ENF and one without. The one without ENF will now be correctly ignored.)

### 1.9.1.5

* Ignore disabled microG versions in the version check
* Set the minimum microG version to 0.2.15 (0.2.16 has additional fixes but it not strictly required)

### 1.9.1.4

* New microg version with some stability fixes
* Check for latest microG version and prompt to update it
* Link to our own FAQ from the App

### 1.9.1.3

* fix crash when opening microG ENF settings
* fix crash during exposure check
* some translation fixes (added polish translation of our additions)

### 1.9.1.2

* Lots of upstream changes, most importantly switching to Exposure Window mode (ENF v2)
* No more gray cards and minimum tracing time. The app will report a risk right away now
* Make the microG UI for exposures available (through the ENF version number in App Information)
* Allow exporting the exposure db for analysis in another app
* Fix for exposure checks sometimes taking very long
* Directly ask for battery optimization exception instead of just sending to settings
* Android 5 support

### 1.7.1.2

* Rebuild for F-Droid reproducible builds

### 1.7.1.1
Initial release of Corona Contact Tracing Germany
