package de.rki.coronawarnapp.ui.launcher

import android.app.Activity
import android.content.Intent

sealed class LauncherEvent {
    object GoToOnboarding : LauncherEvent()
    object GoToMainActivity : LauncherEvent()
    data class ShowMicroGUpdateDialog(
        val updateIntent: Intent
    ) : LauncherEvent()

    data class ForceUpdate(
        val forceUpdate: (Activity) -> Unit
    ) : LauncherEvent()
    object ShowUpdateDialog : LauncherEvent()

    object ShowRootedDialog : LauncherEvent()

    object RestartApp : LauncherEvent()

    data class ErrorToast(val message: String) : LauncherEvent()
}
