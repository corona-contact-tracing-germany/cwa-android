– Registration of Rapid PCR Tests and of multiple tests for family members (CWA)
– Error-tolerant certificate grouping (CWA)
– Support certificate update to new schema version (CWA)
– Expire high risk sooner (CWA)
– Don't hide risk card while tested PCR-positive (CWA)
– Add notifications on status change and on new exposure (CWA)
– Duplicate QR Code Removed (CWA)
– Material Design 3 (CWA)
– Don't request location permission on Android 12
– Fix timezone issues in risk card
