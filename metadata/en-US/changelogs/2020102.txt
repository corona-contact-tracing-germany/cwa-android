* Enable resource and code shrinking (reduces apk size by another ~25%)
* Fix non-reproducible build
* Minor UI and text adjustments
