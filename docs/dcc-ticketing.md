### CCTG on Certificate verification during ticketing

[Since version 2.15](https://www.coronawarn.app/en/blog/2021-12-20-cwa-2-15/), the Corona-Warn-App contains a feature that enables users to submit certificates to an online verification provider ("verification partner") while booking a service online, like a ticket for a flight. This feature is called DCC Ticketing for short, because it allows to proof that a user has a DCCs (Digital Covid Certificats) while booking a ticket.

After the release, multiple aspects of this feature have been criticized, including [that it supposedly reduces the anonymity of users](https://netzpolitik.org/2022/update-der-corona-warn-app-neue-impfstatus-pruefung-auf-kosten-der-anonymitaet/). Some users have consequently asked us whether CCTG was going to include this feature.

Therefore, we considered it a good idea to discuss this topic in detail.

**As of April 2022, this feature is generally considered to be on indefinite hold.**

#### What exactly is DCC Ticketing?

Assume that there is a vendor who would like to offer a service to you as a customer, and determines that a proof of vaccination, recovery or negative test is required. As a service to reduce queue time, it is offered to validate your certificate beforehand during booking.

To implement this procedure, your client application contains DCC Ticketing support. The workflow then works as follows:

* The vendor at which you would like to book a ticket requests a verification provider to validate that you have a certificate of vaccination, recovery or negative test.
* The provider generates a QR code and displays it to you.
* Using this QR code, you upload your certificate to the provider through your app.
* The provider verifies that the certificate is valid and that the conditions requested by the vendor are met.
* The provider deletes the certificate and reports success to the vendor.

No verification providers are open for business in Germany at this moment.

#### EU standard and rollout in Germany

Whilst largely unnoticed by the public, DCC Ticketing [was introduced as an EU specification](https://ec.europa.eu/health/system/files/2021-10/covid-certificate_traveller-onlinebooking_en_0.pdf) within the scope of the eHealth Network's effort to define common interfaces in the spirit of cross-country interoperability in October 2021.¹ Chapter 3 of this document contains details about the procedure used in DCC Ticketing which is also described above.

However, there has been very poor communication about the rollout in Germany beforehand, and even the most dedicated community members were puzzled when seeing Pull Requests regarding this feature to repositories of the Corona-Warn-App.

Even after the launch of version 2.15 that included support for DCC Ticketing, [it was still not communicated](https://github.com/corona-warn-app/cwa-website/issues/2218) how vendors or event organizers could use this feature, or how independent verification services could be implemented and become approved.

It furthermore looks like [T-Systems](https://www.t-systems.com/de/en/newsroom/news/corona-validation-service-475486) had a head-start in implementing a verification service because they knew that it was coming to Corona-Warn-App, and had a better view over what was necessary for implementing it. Shortly after launch of the release, the [FAQ](https://www.coronawarn.app/en/faq/#val_service_basics) was updated with a reference to "a test procedure at the BMG" that was also open to competitors, but without further details or references. Confusingly, on January 10th 2022, the [blog post](https://www.coronawarn.app/en/blog/2021-12-20-cwa-2-15/) noted that no verification service was approved in Germany yet, and that the feature could thus not yet be used.

For obvious reasons, we would very much prefer if it was announced ahead of time what the Corona-Warn-App team was working on, and if feedback from the community – which it does happily provide – was considered before an implementation was started.

Since April 2022, the community considers DCC Ticketing to be on indefinite hold, as there are [no verification providers available](https://github.com/corona-warn-app/cwa-website/issues/2218#issuecomment-1094809124) and the feature thus cannot be used.

#### Privacy properties of DCC Ticketing

It is true that the vendor will not learn details about the certificate during the verification process. However, the verification provider will receive the entire digital certificate, and could theoretically – possibly because of an infiltration by a bad actor – store instead of discard them after verification is completed.

At first, this may sound unavoidable, but those familiar with cryptography theory are able to demonstrate that a [zero-knowledge proof](https://en.wikipedia.org/wiki/Zero-knowledge_proof#The_Ali_Baba_cave) could have been used to prevent the verification provider from being able to reconstruct the valid certificate, while still allowing them to be convinced that the user is in possession of one.²

If the feature was implemented as such a zero-knowledge protocol, it would also not have been necessary to impose special restrictions on the verification providers. This could also allow vendors to perform the validation procedure themselves.

We would have liked to see at least an evaluation of how well this would have worked in practice, and are disappointed of the privacy properties that sending the certificate to the verification service provider holds.


#### Why does CCTG contain DCC Ticketing?

However, we don't see good reasons to exclude the DCC Ticketing feature from the CCTG application.

* The feature is not promoted inside the app and can only be invoked by scanning an appropriate QR code.
* Whether or not CCTG supports DCC Ticketing will barely influence whether vendors will choose to use DCC Ticketing.
* Removing the feature would inconvenience users who want to use DCC Ticketing. They might consort to more complicated and potentially unsafe methods of uploading the certificate, for instance using a screenshot.³

To ensure that users are aware that they are sending their full certificates to a provider, we have decided to adapt the consent text that is shown inside the application before the certificate is sent.

We hope that this document provides a valuable contribution towards the discussion about this feature, and that the solution we implemented in CCTG is able to satisfy all of our users.



---



¹ It had already been referenced earlier, for instance in [the April 2021 version of the specification for the then so-called "Digital Green Certificate"](https://ec.europa.eu/health/system/files/2021-04/digital-green-certificates_v4_en_0.pdf), as "User Story 4: Verifying DGC by an airline", though the section was left to be filled in future versions.

² As unbelievable as it may sound, besides the question about practical feasibility, a protocol that hides arbitrary information from the verification provider also exists. For instance, not only the signature, but also the date of vaccination as well as the vaccine that was administered could be hidden while still guaranteeing that they match the applicable validation rules and that the certificate matches the name that the booking was requested for.

³ Such systems where users are requested to upload a screenshot of their certificate for automatic validation already exist today.
