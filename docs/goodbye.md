
## GOODBYE

Dear community,

as [the Corona-Warn-App infrastructure is shut down and its development ceases](https://www.coronawarn.app/en/faq/results/?search=&topic=end_of_development), CCTG is also sunset after almost exactly [two and a half years of development](https://gitlab.com/fdroid/rfp/-/issues/1387#note_454742464). Thank you for sticking around with us until the end!

We have a few more thoughts to share with you – regarding our relation to the Corona-Warn-App project, about CCTG itself, and a few more words of "thank you".

### Corona-Warn-App is an open source project

We believe that the Corona-Warn-App project is one of the first projects initiated by the German government that truly deserves to be called "open source"; not only in technicality, that is, releasing the source code, but also in spirit: an observable development process that enables participation by outsiders.

The project has planned on interacting with its community since the very beginning, and dedicated a community management team to the task of handling community feedback through the various channels (store reviews, issue trackers).

Of course, there is still lots of room for improvement. For instance, we consider the use of an additional, private issue tracker to be unnecessary. We understand that GitHub's issue system might now satisfy all needs of the project, but we also think that the internal JIRA system should have been open to the public. Because it was private, the community frequently felt left out upon important discussions and developments, even though the community managers were trying their best to mirror new developments back and forth. Generally speaking, we notice that the process of copying issues opened by the community to this internal system lead to redundant work and maintenance overhead. We encourage future projects to consider these points and to improve upon them.

We furthermore would like the community to be involved better in the process of decision-making regarding new app developments, and [for technical specifications to be published](https://github.com/corona-warn-app/cwa-wishlist/issues/376#issuecomment-788653096) sooner, to receive community feedback before the implementation begins.

Still – and this is a very good comparison – we observe that for CovPass, barely any resources were dedicated to "living the open-source spirit", leading to symptoms like a non-public code review process, [delayed source releases](https://github.com/Digitaler-Impfnachweis/covpass-android/issues?q=author%3AEin-Tim+missing+release) and ultimately only [very few external contributions](https://github.com/Digitaler-Impfnachweis/covpass-android/graphs/contributors).

In contrast, [dozens of our contributions](https://github.com/corona-warn-app/cwa-app-android/pulls?q=is%3Apr+author%3Afynngodau) were accepted by the Corona-Warn-App project. We emphasize very strongly that, without an open development process, the Corona-Warn-App project would not have been able to benefit from these and other community fixes and improvements.

In the same way, CCTG as a project strongly benefited from Corona-Warn-App's source code and permissive license as well. Without this, CCTG would have been a very different application, resulting in an entirely detached development and duplicate effort – it would likely not have been able to support all the features that Corona-Warn-App has.

### Replacing proprietary Google code

That praise being said, we can't leave out the most important criticism – the sole reason CCTG exists: that the Corona-Warn-App relied on Google's proprietary Exposure Notifications implementation. We think that, unfortunately, the Corona-Warn-App project has never critically considered whether the dependency on proprietary Google code was really needed; and as you know, we have demonstrated that it was not.

We would have strongly preferred to see official support for devices without Google Play services, such that our community effort would not have been necessary.

### The long-term problems of CCTG

One of the problems we were constantly facing was that on some devices, our scanner service was stopped by the system for no apparent reason. [Quite a few](https://codeberg.org/corona-contact-tracing-germany/cwa-android/issues?q=&type=all&state=open&labels=14536&milestone=0&assignee=0&poster=0) issues that we were not able to solve have been opened about this. Theoretically, the Android system should not restrict background services if battery optimizations are turned off for our app. For the majority of users, this has indeed led to uninterrupted app usage. For the rest of the installations, it would probably have worked better if a foreground service with a persistent notification was implemented (like you know from many other apps, such as Element or K-9 Mail) or could have been enabled manually.¹

Additionally, it might have been useful if we had provided a migration path for users that are switching between internal and external microG. Without this, users of internal microG who decide to install external microG on their system cannot use their previously collected and sent IDs anymore.

A problem that was out of our control was the SafetyNet mechanism used to gate the event-driven survey (displayed upon a red risk card in the Corona-Warn-App), the privacy preserving analytics (data donation) and, most recently, warning others without an in-app test result. Whilst [is not really apt as an anti-abuse tool](https://github.com/corona-warn-app/cwa-wishlist/issues/356#issuecomment-773020760), it indeed is annoying to circumvent and we did not ship a client-side implementation for it. Since [new testing sites could no longer sign up to transferring test results to the app after mid 2022](https://netzpolitik.org/2022/digitale-testinfrastruktur-der-teufelskreis/), many users were then no longer able to warn others when using CCTG.

### Our special thanks

Lastly, we want to speak out our thanks to those involved with the project.

We would like to extend our thanks to the community members that participated in our or in microG's repository. Specifically, we are very grateful to [Elsensee](https://codeberg.org/Elsensee) for finding a [risk calculation issue](https://codeberg.org/corona-contact-tracing-germany/cwa-android/src/branch/main/docs/risk-calculation-issue.md) in microG's code.

Furthermore, we say our thanks to the members that were active in our issue tracker, as well as our Matrix / XMPP room. Similarly, we would like to thank the broader Corona-Warn-App community for its participation upstream and in the COVID apps Slack. Very special thanks go to [Ein-Tim](https://github.com/Ein-Tim), a most active member² – he knows everything about the project and is always happy to answer any question within seconds.

We also want to thank the developers and community managers and other members of the Corona-Warn-App project team at SAP, T-Systems and RKI. Specifically, we want to thank [d4rken](https://github.com/d4rken) for advocating the open-source spirit inside this team and to [Heinezen](https://github.com/Heinezen) for popping into our channel and occasionally answering a question or two.

Finally, we want to thank our donors who supported the CCTG team – Fynn, Bubu and larma – financially. Thanks to anyone who promoted the app to their friends, who provided check-in QR codes at events, who warned others upon a positive test result, who bothered to call the TAN hotline if needed, who acted responsibly upon receiving a red risk warning, and in general, thanks to everyone who used our app.

Thank you all for your efforts in preventing new infections.

Sincerely yours,  
Corona Contact Tracing Germany.

---

¹ microG has an implementation where a foreground service with persistent notification is used if an automatic mechanism detects that this is necessary.

² Since early 2023, Tim is affiliated with RKI. Congratulations on this opportunity.

---

🎲: [📖](https://github.com/corona-warn-app/cwa-app-android/issues/4790#issuecomment-1031893443) [🥚](https://github.com/corona-warn-app/cwa-wishlist/issues/436) [🐦](https://social.tchncs.de/@CCTG/106189908733143352)