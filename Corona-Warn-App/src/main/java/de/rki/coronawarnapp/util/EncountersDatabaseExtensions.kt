package de.rki.coronawarnapp.util

import org.microg.gms.nearby.exposurenotification.ExposureDatabase
import java.time.Instant
import java.time.LocalDate
import java.time.ZoneId

/**
 * @return The amount of collected IDs per epoch day.
 */
fun ExposureDatabase.rpiEpochDaysMap() = LinkedHashMap<Long, Long>().mutate {

    // Generate empty past 15 days
    val today = LocalDate.now()
    for (i in -14..0) {
        put(
            today.plusDays(i.toLong()).toEpochDay(),
            0
        )
    }

    // Sort hourly RPI data into appropriate days
    rpiHourHistogram.forEach {
        val day = Instant.ofEpochMilli(it.time).atZone(ZoneId.systemDefault()).toLocalDate().toEpochDay()
        put(day, (get(day) ?: 0) + it.rpis)
    }
}

