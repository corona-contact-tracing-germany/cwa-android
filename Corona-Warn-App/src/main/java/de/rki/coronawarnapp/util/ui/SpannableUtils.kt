package de.rki.coronawarnapp.util.ui

import android.content.Context
import android.graphics.drawable.AdaptiveIconDrawable
import android.graphics.drawable.Drawable
import android.os.Build
import android.text.SpannableStringBuilder
import android.text.style.DynamicDrawableSpan
import android.text.style.ImageSpan
import android.util.TypedValue
import de.rki.coronawarnapp.util.ExposureNotificationProvider.Companion.MICROG_PACKAGE_NAME

fun createMicrogImageSpan(context: Context): ImageSpan {

    // microG icon drawable
    val packageManager = context.packageManager
    var drawable: Drawable = packageManager.getApplicationIcon(MICROG_PACKAGE_NAME)

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O && drawable is AdaptiveIconDrawable) {
        drawable = drawable.foreground
    }

    // it's important to call setBounds otherwise the image will
    // have a size of 0px * 0px and won't show at all
    val targetSize = spToPx(16f, context)
    drawable.setBounds(0, 0, targetSize, targetSize)

    // now we create a new ImageSpan
    return ImageSpan(drawable, MICROG_PACKAGE_NAME, DynamicDrawableSpan.ALIGN_BASELINE)
}

/**
 * Replaces the given char in the given builder with the given String
 * and spans the given Object over the newly inserted text.
 */
fun replaceWithFormatted(
    builder: SpannableStringBuilder, replace: Char,
    replaceWith: String, spanOnReplacement: Any?
): SpannableStringBuilder {
    // Find char to replace
    for (i in 0 until builder.length) {
        if (builder[i] == replace) {
            builder.delete(i, i + 1)
            builder.insert(i, replaceWith)
            builder.setSpan(spanOnReplacement, i, i + replaceWith.length, 0)
            return builder
        }
    }
    throw IllegalArgumentException("Symbol $replace not in builder")
}

fun spToPx(sp: Float, context: Context): Int {
    return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, context.resources.displayMetrics)
        .toInt()
}
