* Contact journal can now record additional information (duration, etc.)(upstream CWA)
* Add contact journal launcher shortcut (upstream CWA)
* Some more details about the risk status are now displayed (upstream CWA)
