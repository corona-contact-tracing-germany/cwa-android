package de.rki.coronawarnapp.rootdetection.core

import android.content.Context
import com.scottyab.rootbeer.RootBeer
import de.rki.coronawarnapp.util.coroutine.AppScope
import de.rki.coronawarnapp.util.coroutine.DispatcherProvider
import de.rki.coronawarnapp.util.di.AppContext
import de.rki.coronawarnapp.util.flow.shareLatest
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RootDetectionCheck @Inject constructor(
    private val rootBeer: RootBeer,
    private val dispatcherProvider: DispatcherProvider,
    @AppContext private val context: Context,
    @AppScope private val appScope: CoroutineScope
) {
    // Check should run in a background thread cause it uses I/O
    suspend fun isRooted() = withContext(dispatcherProvider.IO) {
        Timber.d("isRooted()")
        isRooted
            .also { Timber.d("Device is rooted: %s", it) }
    }

    private val isRooted: Boolean
        get() = try {
            // this is the all-in-one check upstream uses and we don't really want:
            //rootBeer.isRooted
            // These checks *should* be enough for us and fairly safe?
            rootBeer.detectRootManagementApps() || rootBeer.checkForMagiskBinary() || rootBeer.checkForSuBinary()
        } catch (e: Exception) {
            Timber.e(e, "Root detection failed. Returning false")
            false
        }

    private val isCardDismissedUpdates = Channel<Boolean>()

    val shouldShowRootCard: Flow<Boolean> = flow {

        val isDismissed = isRootCardDismissed()
        var cacheIsRooted = false

        if (!isDismissed) {
            cacheIsRooted = isRooted()
        }

        emit(!isDismissed && cacheIsRooted)

        while (true) {
            // Wait for new dismissed status
            try {
                val cardDismissed = withContext(Dispatchers.IO) {
                    isCardDismissedUpdates.receive()
                }
                emit(!cardDismissed && cacheIsRooted)
            } catch (e: CancellationException) {
                Timber.d("shouldShowRootCard was cancelled")
                break
            }
        }
    }
        .distinctUntilChanged()
        .onCompletion {
            when {
                it is CancellationException -> Timber.d("shouldShowRootCard canceled.")
                it != null -> Timber.e(it, "shouldShowRootCard failed.")
            }
        }
        .shareLatest(
            tag = "shouldShowRootCard",
            scope = appScope
        )

    private fun isRootCardDismissed(): Boolean {
        val rootPref = context.getSharedPreferences("Root", Context.MODE_PRIVATE)
        val isCardDismissed = rootPref.getBoolean("isRootCardDismissed", false)
        Timber.d("isRootCardDismissed: $isCardDismissed")
        return isCardDismissed
    }

    fun setRootCardDismissed(value: Boolean = true) {

        Timber.d("setRootCardDismissed: $value")
        val rootPref = context.getSharedPreferences("Root", Context.MODE_PRIVATE)
        with(rootPref.edit()) {
            putBoolean("isRootCardDismissed", value)
            apply()
        }
        appScope.launch{
            isCardDismissedUpdates.send(value)
        }
    }
}
