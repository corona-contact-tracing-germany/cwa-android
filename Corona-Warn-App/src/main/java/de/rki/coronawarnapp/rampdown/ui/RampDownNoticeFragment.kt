package de.rki.coronawarnapp.rampdown.ui

import android.app.AlertDialog
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.Intent.ACTION_VIEW
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import de.rki.coronawarnapp.R
import de.rki.coronawarnapp.databinding.FragmentRampdownNoticeBinding
import de.rki.coronawarnapp.util.convertToHyperlink
import de.rki.coronawarnapp.util.ui.popBackStack
import de.rki.coronawarnapp.util.ui.viewBinding

class RampDownNoticeFragment : Fragment(R.layout.fragment_rampdown_notice) {

    private val binding: FragmentRampdownNoticeBinding by viewBinding()
    private val navArgs by navArgs<RampDownNoticeFragmentArgs>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val data = navArgs.rampDownNotice

        binding.apply {
            toolbar.title = data.title
            toolbar.setNavigationOnClickListener { popBackStack() }
            rampDownNoticeSubtitle.text = data.subtitle
            rampDownNoticeLongtext.text = data.description
            data.faqUrl?.let {
                rampDownNoticeFaqAnchor.convertToHyperlink(it)
                rampDownNoticeFaqAnchor.isVisible = true
            }
            cctg.setOnClickListener {
                try {
                    startActivity(Intent(ACTION_VIEW, Uri.parse("https://bubu1.eu/cctg/goodbye")))
                } catch (e: ActivityNotFoundException) {
                    AlertDialog.Builder(requireContext()).setMessage("https://bubu1.eu/cctg/goodbye")
                    e.printStackTrace()
                }
            }
        }
    }
}
