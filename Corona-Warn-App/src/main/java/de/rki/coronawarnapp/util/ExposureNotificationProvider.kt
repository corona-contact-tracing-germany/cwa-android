package de.rki.coronawarnapp.util

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.net.Uri
import com.google.android.gms.nearby.exposurenotification.ExposureNotificationClient.ACTION_EXPOSURE_NOTIFICATION_SETTINGS
import de.rki.coronawarnapp.BuildConfig
import de.rki.coronawarnapp.util.di.AppContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.microg.gms.nearby.exposurenotification.ExposureDatabase
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ExposureNotificationProvider @Inject constructor(
    @AppContext private val context: Context
) {

    fun makeExternalExposureNotificationsIntent(): Intent {
        val intent = Intent(ACTION_EXPOSURE_NOTIFICATION_SETTINGS)
        intent.component = ComponentName(MICROG_PACKAGE_NAME, "org.microg.gms.ui.SettingsActivity")
        return intent
    }

    fun makeExternalExposureNotificationsRpisIntent(): Intent {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse("x-gms-settings://exposure-notifications-rpis")
        return intent
    }

    fun getExposureNotificationProviderStatus(): Status {

        // Test if package installed
        lateinit var packageInfo: PackageInfo
        try {
            packageInfo = context.packageManager.getPackageInfo(MICROG_PACKAGE_NAME, PackageManager.GET_PERMISSIONS)
        } catch (e: PackageManager.NameNotFoundException) {
            return Status.EXTERNAL_NOT_INSTALLED
        }

        if (packageInfo.permissions == null) {
            return Status.EXTERNAL_NOT_INSTALLED
        }

        /* Now we know microg exists, but does it contain EN?
         * Find out by checking whether it declares the
         * com.google.android.gms.nearby.exposurenotification.EXPOSURE_CALLBACK and
         * org.microg.gms.EXTENDED_ACCESS permission.
         */
        var isFullMicroG = false
        var hasEN = false
        packageInfo.permissions.forEach {
            if (it.name == MICROG_PERMISSION) isFullMicroG = true
            if (it.name == EN_PERMISSION) hasEN = true
        }

        /* A certain operating system ships with microG that declares the permission,
         * but does not actually implement EN. We detect this by checking the version
         * name whether it contains "noen".
         */
        if ("noen" in packageInfo.versionName) {
            Timber.i("microG version name contains \"noen\" (/e/ hack): ${packageInfo.versionName}")
            return Status.EXTERNAL_NO_EN
        }

        if (!isFullMicroG || !hasEN) {
            // MicroG does not provide the permissions that it would if it were to provide EN
            return Status.EXTERNAL_NO_EN
        }

        /* Test if package enabled
         * We test whether microG package provides EN with higher precedence because it is the more important
         * reason why it couldn't be used.
         */
        if (
            !(context.packageManager.getApplicationEnabledSetting(MICROG_PACKAGE_NAME) == PackageManager.COMPONENT_ENABLED_STATE_DEFAULT
                || context.packageManager.getApplicationEnabledSetting(MICROG_PACKAGE_NAME) == PackageManager.COMPONENT_ENABLED_STATE_ENABLED)
        )
            return Status.EXTERNAL_DISABLED

        // Test version
        val currentVersion = packageInfo.versionCode

        val needsImmediateUpdate = currentVersion < BuildConfig.MINIMUM_MICROG_VERSION

        Timber.d("Current microG version: $currentVersion")
        Timber.d("Required microG version: ${BuildConfig.MINIMUM_MICROG_VERSION}")

        return if (needsImmediateUpdate) {
            Timber.e("Needs update to version ${BuildConfig.MINIMUM_MICROG_VERSION} (${BuildConfig.MINIMUM_MICROG_VERSION_NAME})!")
            Status.EXTERNAL_UPDATE_REQUIRED
        } else {
            Status.EXTERNAL_USED
        }
    }

    fun isExternalExposureNotificationsUsed(): Boolean {
        val status = getExposureNotificationProviderStatus()
        return status == Status.EXTERNAL_USED || status == Status.EXTERNAL_UPDATE_REQUIRED
    }

    fun launchExposureNotificationSettings(externalNotAvailable: () -> Unit) {

        if (isExternalExposureNotificationsUsed()) {
            try {
                context.startActivity(makeExternalExposureNotificationsIntent())
            } catch (e: Exception) {
                Timber.e(e, "External microg unexpectedly not available")
                externalNotAvailable()
            }
        } else {
            externalNotAvailable()
        }
    }

    fun canResolveRpiIntent(): Boolean {
        return makeExternalExposureNotificationsRpisIntent().resolveActivity(context.packageManager) != null
    }

    suspend fun <T> accessDatabase(call: suspend (ExposureDatabase) -> T): T = withContext(Dispatchers.IO) {
        ExposureDatabase.with(context) { call.invoke(it) }
    }

    enum class Status {
        EXTERNAL_NOT_INSTALLED, EXTERNAL_DISABLED, EXTERNAL_NO_EN, EXTERNAL_USED, EXTERNAL_UPDATE_REQUIRED
    }

    companion object {
        const val MICROG_PACKAGE_NAME = "com.google.android.gms"
        private const val MICROG_PERMISSION = "org.microg.gms.EXTENDED_ACCESS"
        private const val EN_PERMISSION = "com.google.android.gms.nearby.exposurenotification.EXPOSURE_CALLBACK"
    }
}
